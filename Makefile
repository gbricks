PROGRAM=test

build: $(PROGRAM)

#SOURCES = $(shell find . -name "*.c")
SRC_VALA = $(PROGRAM).vala
SRC_C = $(SRC_VALA:.vala=.c)
OBJS = $(SRC_C:.c=.o)

VALA_PKGS=--pkg goocanvas --pkg librsvg-2.0
VALA_FLAGS=

PKG_CONFIG=goocanvas librsvg-2.0

EXTRA_CFLAGS=-I. `pkg-config --cflags $(PKG_CONFIG)`
CFLAGS= -O2 -g -Wall

TRACE_CFLAGS=-finstrument-functions
TRACE_OBJS=$(SRC_C:.c=.trace.o) trace.trace.o

LDFLAGS= -Wl,-z,defs -Wl,--as-needed -Wl,--no-undefined
EXTRA_LDFLAGS=
LIBS=`pkg-config --libs $(PKG_CONFIG)`

$(PROGRAM): $(OBJS)
	gcc $(LDFLAGS) $(EXTRA_LDFLAGS) $+ -o $@ $(LIBS)

$(PROGRAM).trace: $(TRACE_OBJS)
	gcc $(LDFLAGS) $(EXTRA_LDFLAGS) $+ -o $@ $(LIBS)

%.trace.o: %.c
	gcc -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS) $(TRACE_CFLAGS)

%.o: %.cpp
	g++ -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

%.o: %.c
	gcc -o $@ -c $+ $(CFLAGS) $(EXTRA_CFLAGS)

%.c %.h: %.vala
	valac $(VALA_PKGS) $(VALA_FLAGS) -C $+ -o $@ -H $(@:.c=.h) --symbols=$(@:.c=.symbols)

clean:
	rm -fv *.o *.a *~ trace.out
	rm -fv $(PROGRAM) $(PROGRAM).trace $(PROGRAM).o $(PROGRAM).c $(PROGRAM).h $(PROGRAM).symbols


