using Gtk;
using Goo;
using Rsvg;
using Cairo;

// https://live.gnome.org/Vala/Tutorial
// https://live.gnome.org/Vala/GTKSample
// http://www.vala-project.org/doc/docu/goocanvas.vapi/index.html

public class SVGItem : Goo.CanvasItemSimple {
    private double x;
    private double y;
    private double height;
    private double width;
    private Rsvg.Handle rsvg_handle;

    public SVGItem (double px, double py, Rsvg.Handle handle) {
        stdout.printf("SVGItem::SVGItem\n");
        x = px;
        y = py;
        width = handle.width;
        height = handle.height;
        rsvg_handle = handle;
        stdout.printf(" h=%lf, w=%lf\n", height, width);
    }

    public override void simple_paint (Cairo.Context cr, Goo.CanvasBounds bounds) {
        stdout.printf("SVGItem::simple_paint\n");
        Cairo.Matrix matrix;
        cr.get_matrix(out matrix);
        matrix.translate(x, y);
        cr.set_matrix(matrix);
        rsvg_handle.render_cairo(cr);
        cr.move_to(x, y);
        cr.line_to(x, y + height);
        cr.line_to(x + width, y + height);
        cr.line_to(x + width, y);
        cr.close_path();
    }

    public override void simple_update (Cairo.Context cr) {
        stdout.printf("SVGItem::simple_update\n");
        bounds.x1 = x;
        bounds.x2 = x + width;
        bounds.y1 = y;
        bounds.y1 = y + height;
    }

    public override bool simple_is_item_at (double px, double py, Cairo.Context cr, bool is_pointer_event) {
        stdout.printf("SVGItem::simple_is_item_at\n");
        if (px < x || (px > x + width) || py < y || (py > y + height))
        {
            return false;
        } else {
            return true;
        }
    }
}

int main (string[] args) {
    Gtk.init (ref args);

    var window = new Window ();
    window.title = "Test Program";
    window.window_position = WindowPosition.CENTER;
    window.set_default_size (640, 600);
    window.destroy.connect (Gtk.main_quit);

    var scroll = new ScrolledWindow (null, null);
    scroll.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
    window.add (scroll);

    var canvas = new Canvas ();
    canvas.automatic_bounds = true;
    scroll.add (canvas);

    var root = canvas.get_root_item ();

    double zoom = 1.0;
    root.set_simple_transform(0, 0, zoom, 0);

    canvas.button_press_event.connect ((target, event) => {
                    return false;
        });

    canvas.button_release_event.connect ((target, event) => {
                    return false;
        });

    canvas.motion_notify_event.connect ((target, event) => {
                    return false;
        });

    canvas.scroll_event.connect ((target, event) => {
                    if (event.direction == Gdk.ScrollDirection.UP || event.direction == Gdk.ScrollDirection.LEFT)
                    {
                      zoom *= 0.9;
                    } else {
                      zoom /= 0.9;
                    }
                    root.set_simple_transform(0, 0, zoom, 0);
                    return false;
        });

    var red_svg = new Rsvg.Handle.from_file("images/redCircle.svg");

    var red_item = new SVGItem (10, 10, red_svg);
    red_item.set_parent(root);
    root.add_child(red_item, 0);

    var rect_item = CanvasRect.create(root, 100, 100, 400, 400,
                                   "line-width", 10.0,
                                   "radius-x", 20.0,
                                   "radius-y", 10.0,
                                   "stroke-color", "yellow",
                                   "fill-color", "red");

    bool dragging = false;
    double drag_x = 0;
    double drag_y = 0;

    rect_item.button_press_event.connect ((target, event) => {
                    stdout.printf("Pressed\n");
                    if (event.button == 1)
                    {
                        drag_x = event.x;
                        drag_y = event.y;
                        dragging = true;
                        return true;
                    }
                    return false;
        });

    rect_item.button_release_event.connect ((target, event) => {
                    stdout.printf("Released\n");
                    dragging = false;
                    return true;
        });

    rect_item.motion_notify_event.connect ((target, event) => {
                    if (dragging)
                    {
                        double new_x = event.x;
                        double new_y = event.y;
                        target.translate(new_x - drag_x, new_y - drag_y);
                        return true;
                    }
                    return false;
        });

    window.show_all ();

    Gtk.main ();
    return 0;
}

