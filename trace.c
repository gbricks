#include <stdio.h>
#include <time.h>

/* See: http://balau82.wordpress.com/2010/10/06/trace-and-profile-function-calls-with-gcc/ */

# ifdef __cplusplus
extern "C" {
# endif

static FILE *fp_trace;

void __attribute__ ((constructor)) trace_begin (void) __attribute__ ((no_instrument_function));
void __attribute__ ((destructor)) trace_end (void) __attribute__ ((no_instrument_function));
void __cyg_profile_func_enter( void *, void * ) __attribute__ ((no_instrument_function));
void __cyg_profile_func_exit( void *, void * ) __attribute__ ((no_instrument_function));


void __attribute__ ((constructor)) trace_begin (void)
{
  fp_trace = fopen("trace.out", "w");
}

void __attribute__ ((destructor)) trace_end (void)
{
  if(fp_trace != NULL) {
    fclose(fp_trace);
  }
}

void __cyg_profile_func_enter (void *func,  void *caller)
{
  if(fp_trace != NULL) {
    fprintf(fp_trace, "e %p %p %lu\n", func, caller, time(NULL) );
  }
}

void __cyg_profile_func_exit (void *func, void *caller)
{
  if(fp_trace != NULL) {
    fprintf(fp_trace, "x %p %p %lu\n", func, caller, time(NULL));
  }
}

# ifdef __cplusplus
}
# endif

